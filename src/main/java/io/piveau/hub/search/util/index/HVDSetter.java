package io.piveau.hub.search.util.index;

import io.piveau.hub.search.*;
import io.piveau.json.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Supports to mark a Dataset as HVD.
 * It sets the property is_hvd depending on the correct values of the property applicable_legislation.
 * applicable_legislation can be set in a Dataset, Distribution or Data Service
 */
public class HVDSetter {
    private final String legislationField;
    private final String hvdELI;
    private final boolean hvdInMapping;
    private final String isHvdKey;
    private final Integer level;
    public HVDSetter(IndexManager indexManager, JsonObject config) {
        this.legislationField = "applicable_legislation";
        this.hvdELI = "http://data.europa.eu/eli/reg_impl/2023/138/oj";
        this.isHvdKey = "is_hvd";
        JsonObject datasetMapping = indexManager.getMappings().get("dataset");
        // True if is_hvd is present in the current Mapping
        hvdInMapping = datasetMapping.getJsonObject("properties").containsKey(isHvdKey);
        level = config.getInteger(Constants.ENV_PIVEAU_HVD_SETTER_LEVEL, 3);
        // Level 1 - Only Dataset
        // Level 2 - Dataset and Distributions
        // Level 3 - Dataset, Distributions and Data Services
    }

    public void set(JsonArray payload) {
        if(hvdInMapping) {
            payload.stream()
                    .map(JsonObject.class::cast)
                    .forEach(this::set);
        }
    }

    public void set(JsonObject payload) {
        if(hvdInMapping) {
            payload.put(isHvdKey, checkAllProperties(payload));
        }
    }

    public void setWithPatch(JsonObject fullDataset, JsonObject patch) {
        fullDataset.mergeIn(patch);
        if(hvdInMapping) {
            patch.put(isHvdKey, checkAllProperties(fullDataset));
        }
    }

    private boolean checkAllProperties(JsonObject payload) {
        String distroKey = "distributions" ;
        String serviceKey = "access_service" ;

        if(checkForHvdELI(payload)) {
            return true;
        }

        if (level > 1 && payload.containsKey(distroKey) && payload.getValue(distroKey) instanceof JsonArray) {
            JsonArray distributions = payload.getJsonArray(distroKey);
            return distributions.stream()
                    .filter(JsonObject.class::isInstance)
                    .map(JsonObject.class::cast)
                    .anyMatch(dist -> checkForHvdELI(dist) ||
                            (level > 2 && dist.containsKey(serviceKey) && dist.getValue(serviceKey) instanceof JsonArray &&
                                    dist.getJsonArray(serviceKey).stream()
                                            .filter(JsonObject.class::isInstance)
                                            .map(JsonObject.class::cast)
                                            .anyMatch(this::checkForHvdELI)));
        }
        return false;
    }
    private boolean checkForHvdELI(JsonObject payload) {
        return payload.containsKey(legislationField)
                && payload.getValue(legislationField) instanceof JsonArray
                && (payload.getJsonArray(legislationField).contains(hvdELI));
    }

}
