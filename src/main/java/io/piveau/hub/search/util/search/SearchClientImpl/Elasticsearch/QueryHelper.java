package io.piveau.hub.search.util.search.SearchClientImpl.Elasticsearch;

import co.elastic.clients.elasticsearch._types.CoordsGeoBounds;
import co.elastic.clients.elasticsearch._types.FieldValue;
import co.elastic.clients.elasticsearch._types.GeoBounds;
import co.elastic.clients.elasticsearch._types.query_dsl.*;
import co.elastic.clients.json.JsonData;
import io.piveau.hub.search.Constants;
import io.piveau.hub.search.Helper;
import io.piveau.hub.search.util.geo.BoundingBox;
import io.piveau.hub.search.util.request.SearchParams;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.*;
import java.util.stream.Stream;

public class QueryHelper extends Helper{

    private static final String FIELD = "field";

    public static void addSearchParamsQuery(Map<String, JsonObject> searchParams, SearchParams querySearchParams, BoolQuery.Builder fullQuery) {
        JsonObject scoring = searchParams.get("scoring");
        if (scoring != null)
            addScoring(scoring.getString(FIELD), querySearchParams, fullQuery);

        JsonObject temporal = searchParams.get("temporal");
        if (temporal != null)
            addTemporal(temporal.getString(FIELD), querySearchParams, fullQuery);

        JsonObject spatial = searchParams.get("spatial");
        if (spatial != null)
            addSpatial(spatial.getString(FIELD), querySearchParams, fullQuery);

        JsonObject countryData = searchParams.get("countryData");
        if (countryData != null && querySearchParams.getCountryData() != null)
            addCountryData(countryData, querySearchParams, fullQuery);

        JsonObject dataServices = searchParams.get("dataServices");
        if (dataServices != null && querySearchParams.getDataServices() != null)
            addDataServices(dataServices, querySearchParams, fullQuery);
    }

    public static void handleFacetQuery(Map<String, String[]> queryFacets, Constants.Operator queryFacetOperator,
                                        Constants.Operator queryFacetGroupOperator, Map<String, JsonObject> facets,
                                        String facetName, BoolQuery.Builder facetQuery) {
        BoolQuery.Builder subQuery = new BoolQuery.Builder();

        if (facets.containsKey(facetName)) {
            processSubQueryWithFacets(queryFacets, queryFacetOperator, facets, facetName, subQuery);
        } else {
            processSubQuery(queryFacets, queryFacetOperator, facetName, subQuery);
        }

        if (Constants.Operator.AND.equals(queryFacetGroupOperator)) {
            facetQuery.must(subQuery.build()._toQuery());
        } else {
            facetQuery.should(subQuery.build()._toQuery());
        }
    }

    private static void addScoring(String scoreField, SearchParams querySearchParams, BoolQuery.Builder fullQuery) {
        if (isNullOrEmpty(scoreField)) return;

        RangeQuery.Builder scoreQueryBuilder = QueryBuilders.range().field(scoreField);
        Query scoreQuery = buildRangeQuery(scoreQueryBuilder, querySearchParams.getMinScoring(),
                querySearchParams.getMaxScoring());
        if (scoreQuery != null)
            fullQuery.must(scoreQuery);
    }

    private static void addTemporal(String dateField, SearchParams querySearchParams, BoolQuery.Builder fullQuery) {
        if (isNullOrEmpty(dateField)) return;

        RangeQuery.Builder dateQueryBuilder = QueryBuilders.range().field(dateField);
        Query dateQuery = buildRangeQuery(dateQueryBuilder, querySearchParams.getMinDate(),
                querySearchParams.getMaxDate());
        if (dateQuery != null)
            fullQuery.must(dateQuery);
    }

    private static void addSpatial(String spatialField, SearchParams querySearchParams, BoolQuery.Builder fullQuery) {
        if (isNullOrEmpty(spatialField) || querySearchParams.getBoundingBox() == null) return;

        GeoBoundingBoxQuery.Builder geoBbQueryBuilder = new GeoBoundingBoxQuery.Builder();
        BoundingBox boundingBox = querySearchParams.getBoundingBox();
        Float minLon = boundingBox.getMinLon();
        Float maxLon = boundingBox.getMaxLon();
        Float maxLat = boundingBox.getMaxLat();
        Float minLat = boundingBox.getMinLat();

        if (minLon != null && maxLon != null && maxLat != null && minLat != null) {
            GeoBounds geoBounds = GeoBounds.of(builder -> builder
                    .coords(new CoordsGeoBounds.Builder()
                            .top(maxLon).left(minLat).bottom(minLon).right(maxLat).build()));
            geoBbQueryBuilder = new GeoBoundingBoxQuery.Builder()
                    .field(spatialField).boundingBox(geoBounds);
        }

        fullQuery.filter(geoBbQueryBuilder.build()._toQuery());
    }

    public static void addCountryData(JsonObject countryData, SearchParams querySearchParams, BoolQuery.Builder fullQuery) {
        JsonArray values = countryData.getJsonArray("values");
        if (isNullOrEmpty(values)) return;

        List<FieldValue> fieldValues = new ArrayList<>();
        for ( Object value : values.getList()) {
            fieldValues.add(FieldValue.of(value.toString()));
        }
        Query termsQuery = QueryBuilders.terms().field( "country.id.raw")
                .terms(TermsQueryField.of(tqfBuilder -> tqfBuilder
                        .value(fieldValues)))
                .build()._toQuery();

        if(Boolean.TRUE.equals(querySearchParams.getCountryData())) {
            fullQuery.mustNot(termsQuery);
        } else {
            fullQuery.filter(termsQuery);
        }
    }

    private static void addDataServices(JsonObject dataServices, SearchParams querySearchParams, BoolQuery.Builder fullQuery) {
        String dataServicesField = dataServices.getString(FIELD, "distributions.access_service");
        if(Boolean.TRUE.equals(querySearchParams.getDataServices())) {
            fullQuery.must(QueryBuilders.exists()
                    .field(dataServicesField)
                    .build()._toQuery()
            );
        }
    }

    private static void processSubQuery(Map<String, String[]> queryFacets, Constants.Operator queryFacetOperator,
                                        String facetName, BoolQuery.Builder subQuery) {
        for (String value : queryFacets.get(facetName)) {
            subQuery.should(qBuilder -> qBuilder
                    .term(tqBuilder -> tqBuilder
                            .field(facetName)
                            .value(value)));
        }

        if (!Constants.Operator.AND.equals(queryFacetOperator)) {
            subQuery.minimumShouldMatch(String.valueOf(1));
        }
    }

    private static void processSubQueryWithFacets(Map<String, String[]> queryFacets, Constants.Operator queryFacetOperator,
                                                  Map<String, JsonObject> facets, String facetName, BoolQuery.Builder subQuery) {
        JsonObject facetJson = facets.get(facetName);
        String facetPath = facetJson.getString("path");
        String facetType = facetJson.getString("type");
        Boolean facetPlain = facetJson.getBoolean("plain", false);
        String facetAggregationTerm = facetJson.getString("aggregation_term", "id.raw");

        if (!isNullOrEmpty(facetType)) return;

        String field = Boolean.TRUE.equals(facetPlain) ?
                facetPath : facetPath + "." + facetAggregationTerm;

        if ( queryFacets.get(facetName) == null || queryFacets.get(facetName).length == 0 ) return;

        Stream.of(queryFacets.get(facetName))
                .map(facet -> QueryBuilders.term()
                        .field(field)
                        .value(facet)
                        .build()._toQuery())
                .forEach(termQueryBuilder -> {
                    if (Constants.Operator.AND.equals(queryFacetOperator)) {
                        subQuery.must(termQueryBuilder);
                    } else {
                        subQuery.should(termQueryBuilder);
                        subQuery.minimumShouldMatch(String.valueOf(1));
                    }
                });
    }

    private static Query buildRangeQuery(RangeQuery.Builder queryBuilder, Object minValue, Object maxValue) {
        if (minValue != null && maxValue == null) {
            return queryBuilder.gte(JsonData.of(minValue))
                    .build()._toQuery();
        }
        if (minValue == null && maxValue != null) {
            return queryBuilder.lte(JsonData.of(maxValue))
                    .build()._toQuery();
        }
        if (minValue != null /*&& maxDate != null*/) {
            return queryBuilder.gte(JsonData.of(minValue)).lte(JsonData.of(maxValue))
                    .build()._toQuery();
        }
        return null;
    }

}
