package io.piveau.hub.search.util.search.SearchClientImpl.Elasticsearch;

import co.elastic.clients.elasticsearch._types.FieldValue;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregation;
import co.elastic.clients.elasticsearch._types.query_dsl.*;
import co.elastic.clients.json.JsonData;
import io.piveau.hub.search.Constants;
import io.piveau.hub.search.Helper;
import io.piveau.hub.search.util.request.QueryParams;
import io.piveau.hub.search.util.request.SearchParams;
import io.vertx.core.json.JsonObject;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.queryparser.flexible.standard.StandardQueryParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchQueryBuilder {
    private static final String ID_RAW = ".id.raw";
    private static final String COUNTRY_DATA = "countryData";
    private static final String TITLE = "title";

    private static final Logger LOG = LoggerFactory.getLogger(SearchQueryBuilder.class);

    public static BoolQuery buildQuery(QueryParams query, JsonObject options, Operator operator) {
        BoolQuery.Builder fullQuery = new BoolQuery.Builder();

        Query qQuery = buildQQuery(query, options, operator, null, null);
        fullQuery.must(qQuery);

        return fullQuery.build();
    }

    public static BoolQuery buildQuery(QueryParams query, JsonObject options, Operator operator,
                                       JsonObject boost, Map<String, JsonObject> facets,
                                       Map<String, JsonObject> searchParams) {
        BoolQuery.Builder fullQuery = new BoolQuery.Builder();

        Query qQuery = buildQQuery(query, options, operator, boost, searchParams);
        fullQuery.must(qQuery);

        if (!Helper.isNullOrEmpty(query.getExists())) {
            Query existsQuery = buildExistsQuery(query.getExists());
            fullQuery.must(existsQuery);
        }

        if (Helper.isNullOrEmpty(query.getFilters()) &&
                Helper.isNullOrEmpty(query.getFilter()))
            return fullQuery.build();

        if (!Helper.isNullOrEmpty(query.getFacets())) {
            Query facetQuery = buildFacetQuery(query.getFacets(), query.getFacetOperator(),
                    query.getFacetGroupOperator(), facets);
            fullQuery.must(facetQuery);
        }

        if (query.getSearchParams() == null)
            return fullQuery.build();

        QueryHelper.addSearchParamsQuery(searchParams, query.getSearchParams(), fullQuery);

        return fullQuery.build();
    }

    public static BoolQuery buildGlobalQuery(List<String> queryFilters, SearchParams querySearchParams,
                                             Map<String, JsonObject> searchParams) {
        BoolQuery.Builder fullQuery = QueryBuilders.bool();

        if (queryFilters == null || querySearchParams == null)
            return fullQuery.build();

        JsonObject countryData = searchParams.get(COUNTRY_DATA);
        if (countryData != null && querySearchParams.getCountryData() != null)
            QueryHelper.addCountryData(countryData, querySearchParams, fullQuery);

        return fullQuery.build();
    }

    public static Aggregation genMinAggregation(String path, String title) {
        return Aggregation.of(aBuilder -> aBuilder
                .min(maBuilder -> maBuilder.field(path))
                .meta(genMetaData(title, null, null)));
    }

    public static Aggregation genMaxAggregation(String path, String title) {
        return Aggregation.of(aBuilder -> aBuilder
                .max(maBuilder -> maBuilder.field(path))
                .meta(genMetaData(title, null, null)));
    }

    public static Aggregation genRangeAggregation(String path, String title, Double from, Double to) {
        HashMap<String, JsonData> metaData = new HashMap<>();
        metaData.put(TITLE, JsonData.of(title));
        metaData.put("from", JsonData.of(from));
        metaData.put("to", JsonData.of(to));

        return Aggregation.of(aBuilder -> aBuilder
                .range(raBuilder -> raBuilder
                        .ranges(arBuilder -> arBuilder.from(String.valueOf(from)).to(String.valueOf(to)))
                        .field(path))
                .meta(metaData));
    }

    public static Aggregation genTermsAggregation(String path, String title, String aggregationTerm,
                                                  Integer maxAggSize, Boolean plain) {
        String field = Boolean.TRUE.equals(plain) ? path : path + "." + aggregationTerm;

        return Aggregation.of(aBuilder -> aBuilder
                .terms(atBuilder -> atBuilder
                        .size(maxAggSize)
                        .field(field))
                .meta(genMetaData(title, null, null))
        );
    }

    public static Aggregation genMustMatchAggregation (String path, String title, boolean match, List<FieldValue> values) {
        TermsQuery termsQuery = TermsQuery.of(tqBuilder -> tqBuilder
                .field(path + ID_RAW)
                .terms(tqfBuilder -> tqfBuilder.value(values)));
        Query query;
        if (match) {
            query = QueryBuilders.bool().must(qBuilder -> qBuilder.terms(termsQuery))
                    .build()._toQuery();
        } else {
            query = QueryBuilders.bool().mustNot(qBuilder -> qBuilder.terms(termsQuery))
                    .build()._toQuery();
        }

        return Aggregation.of(builder -> builder
                .filter(query)
                .meta(genMetaData(title, match, "mustMatch")));
    }

    public static Aggregation genMustNotMatchAggregation (String path, String title, boolean match, List<FieldValue> values) {
        TermsQuery termsQuery = TermsQuery.of(tqBuilder -> tqBuilder
                .field(path + ID_RAW)
                .terms(tqfBuilder -> tqfBuilder.value(values)));
        Query query;
        if (match) {
            query = QueryBuilders.bool().mustNot(qBuilder -> qBuilder.terms(termsQuery))
                    .build()._toQuery();
        } else {
            query = QueryBuilders.bool().must(qBuilder -> qBuilder.terms(termsQuery))
                    .build()._toQuery();
        }

        return Aggregation.of(builder -> builder
                .filter(query)
                .meta(genMetaData(title, match, "mustNotMatch")));
    }

    private static Map<String, JsonData> genMetaData(String title, Boolean match, String type) {
        HashMap<String, JsonData> metaData = new HashMap<>();
        metaData.put(TITLE, JsonData.of(title));
        if (match != null)
            metaData.put("match", JsonData.of(match));
        if (type != null)
            metaData.put("type", JsonData.of(type));

        return metaData;
    }

    private static Query buildQQuery(QueryParams queryParams, JsonObject options, Operator operator,
                                     JsonObject boost, Map<String, JsonObject> searchParams) {
        if (Helper.isNullOrEmpty(queryParams.getQ())) {
            return QueryBuilders.matchAll().boost(1.0f).build()._toQuery();
        } else {
            List<String> multiMatchFields = determineMultiMatchFields(queryParams.isAutocomplete(), queryParams.getFields(), boost, searchParams);
            String querystring = queryParams.getQ().trim();
            if (isWildcardQuery(querystring) || hasRegexOperators(querystring)) {
                return buildQueryString(querystring, multiMatchFields, operator);
            } else {
                return buildMultiMatch(querystring, multiMatchFields, options, operator);
            }
        }
    }

    private static boolean hasRegexOperators(String input) {
        final String regexReservedOperators = ".*[.?+*|{}\\[\\]()\"\\\\].*";
        // Regex to match text wrapped in forward slashes
        Matcher matcher = Pattern.compile("/([^/]+)/")
                .matcher(input);
        boolean hasRegexOperators = input.matches(regexReservedOperators);
        boolean hasWrappedPattern = matcher.find();

        return (hasRegexOperators && hasWrappedPattern);
    }

    private static boolean isWildcardQuery(String input) {
        final List<String> wildcardChars = List.of("(", ")", "AND", "OR", "*", "?", "\"");
        final String defaultField = "defaultField";

        if (wildcardChars.stream().anyMatch(input::contains)) {
            try {
                StandardQueryParser queryParser = new StandardQueryParser();
                queryParser.setAllowLeadingWildcard(false);
                queryParser.setAnalyzer(new WhitespaceAnalyzer());
                queryParser.parse(input, defaultField);

                return true;
            } catch (Exception e) {
                return false;
            }
        } else {
            return false;
        }
    }

    private static Query buildExistsQuery(List<String> exists) {
        BoolQuery.Builder existsQuery = new BoolQuery.Builder();
        exists.forEach(exist ->
                existsQuery.must(QueryBuilders.exists()
                        .queryName(exist)
                        .field(exist)
                        .build()._toQuery())
        );

        return existsQuery.build()._toQuery();
    }

    private static Query buildFacetQuery(HashMap<String, String[]> queryFacets, Constants.Operator queryFacetOperator,
                                         Constants.Operator queryFacetGroupOperator, Map<String, JsonObject> facets) {
        BoolQuery.Builder facetQuery = new BoolQuery.Builder();

        queryFacets.keySet().forEach(facetName ->
                QueryHelper.handleFacetQuery(queryFacets, queryFacetOperator, queryFacetGroupOperator, facets, facetName, facetQuery));

        if (queryFacetGroupOperator.equals(Constants.Operator.OR)) {
            facetQuery.minimumShouldMatch(String.valueOf(1));
        }

        return facetQuery.build()._toQuery();
    }

    private static Query buildQueryString(String querystring, List<String> fields, Operator operator) {
        QueryStringQuery.Builder queryString = new QueryStringQuery.Builder();

        queryString.type(TextQueryType.BestFields)
                .defaultOperator(operator)
                .maxDeterminizedStates(10000)
                .enablePositionIncrements(true)
                .fuzziness("AUTO").fuzzyPrefixLength(0).fuzzyMaxExpansions(50)
                .phraseSlop(0d)
                .escape(false)
                .autoGenerateSynonymsPhraseQuery(true)
                .fuzzyTranspositions(true)
                .boost(1.0f);

        if (querystring.startsWith("\"") && querystring.endsWith("\"")) {
            String finalQuerystring = querystring.toLowerCase();
            return queryString.query(finalQuerystring)
                    .fields(fields)
                    .analyzer("whitespace")
                    .build()._toQuery();
        } else {
            return queryString.query(querystring)
                    .fields(fields)
                    .allowLeadingWildcard(false)
                    .analyzeWildcard(true)
                    .build()._toQuery();
        }
    }

    private static Query buildMultiMatch(String querystring, List<String> multiMatchFields, JsonObject options, Operator operator) {

        String finalQuerystring = querystring.trim();

        MultiMatchQuery.Builder multiMatchQuery = new MultiMatchQuery.Builder();

        if (options.containsKey("multi_match_type") && options.getString("multi_match_type").equals("most_fields")) {
            multiMatchQuery.type(TextQueryType.MostFields);
        }
        multiMatchQuery.query(finalQuerystring).fields(multiMatchFields).operator(operator)
                .slop(0).prefixLength(0).maxExpansions(50)
                .lenient(true).zeroTermsQuery(ZeroTermsQuery.None).autoGenerateSynonymsPhraseQuery(true)
                .fuzzyTranspositions(true).boost(1.0f);

        return multiMatchQuery.build()._toQuery();
    }

    private static List<String> determineMultiMatchFields(boolean isQueryAutocomplete, List<String> queryFields, JsonObject boost,
                                                          Map<String, JsonObject> searchParams) {
        List<String> multiMatchFields = new ArrayList<>();

        if (isQueryAutocomplete && searchParams != null) {
            JsonObject autocompletesJson = searchParams.get("autocomplete");
            if (autocompletesJson != null && !autocompletesJson.isEmpty()) {
                autocompletesJson.getJsonArray("values").forEach(value ->
                        multiMatchFields.add(value.toString()+"^1.0")
                );
            }
            return multiMatchFields;
        }

        if (queryFields == null || queryFields.isEmpty()) {
            multiMatchFields.add("*^1.0");
            if (!Helper.isNullOrEmpty(boost)) {
                boost.forEach(item ->
                        multiMatchFields.add(item.getKey()+"^"+item.getValue())
                );
            }
            return multiMatchFields;
        }

        queryFields.forEach(field -> {
            String[] split = field.split("\\^");
            if (split.length == 2) {
                try {
                    float boostValue = Float.parseFloat(split[1]);
                    multiMatchFields.add(split[0]+"^"+boostValue);
                } catch (NumberFormatException e) {
                    multiMatchFields.add(split[0]+"^1.0");
                }
            } else {
                multiMatchFields.add(split[0]+"^1.0");
            }
        });
        return multiMatchFields;
    }

}
