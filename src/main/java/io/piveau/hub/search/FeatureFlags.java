/*
 * Copyright (c) Fraunhofer FOKUS
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package io.piveau.hub.search;

import io.vertx.core.json.*;

public final class FeatureFlags {

    private static JsonObject config =  new JsonObject();

    public static void setConfig(JsonObject config) {
        FeatureFlags.config = config;
    }

    public static Boolean isEnabled(String flag) {
        return FeatureFlags.config.getBoolean(flag, false);
    }

    public static Boolean isSet(String flag) {
        return FeatureFlags.config.containsKey(flag);
    }

}
