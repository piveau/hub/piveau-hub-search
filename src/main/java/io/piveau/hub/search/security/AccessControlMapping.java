/*
 * Copyright (c) Fraunhofer FOKUS
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package io.piveau.hub.search.security;

import io.piveau.hub.search.*;
import io.vertx.core.json.*;


public class AccessControlMapping {

    public void injectProperties(String index, JsonObject mapping) {
        if (index.equals(Constants.DOC_TYPE_CATALOGUE)) {
            mapping.getJsonObject("properties").put("public", new JsonObject().put("type", "boolean"));
        }
        if (index.equals(Constants.DOC_TYPE_DATASET)) {
            mapping.getJsonObject("properties").put("public", new JsonObject().put("type", "boolean"));
        }
    }
}
