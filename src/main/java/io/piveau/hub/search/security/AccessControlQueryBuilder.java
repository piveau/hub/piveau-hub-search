/*
 * Copyright (c) Fraunhofer FOKUS
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package io.piveau.hub.search.security;

import io.piveau.hub.search.util.request.*;
import org.elasticsearch.index.query.*;
import org.slf4j.*;

import java.util.*;

public class AccessControlQueryBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(AccessControlQueryBuilder.class);

    public static void buildQuery(BoolQueryBuilder boolQueryBuilder, QueryParams query) {
        if (AccessControlUtils.isEnabled()) {
            boolQueryBuilder.should(QueryBuilders.termQuery("public", true));
            boolQueryBuilder.should(QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery("public")));

            TermsQueryBuilder termsQuery = QueryBuilders.termsQuery("catalog.id.raw", query.getCatalogIds());
            boolQueryBuilder.should(termsQuery);

            if(query.getDatasetIds() != null && !query.getDatasetIds().isEmpty()) {
                termsQuery = QueryBuilders.termsQuery("id.raw", query.getDatasetIds());
                boolQueryBuilder.should(termsQuery);
            }
            boolQueryBuilder.minimumShouldMatch(1);
        }
    }
}
