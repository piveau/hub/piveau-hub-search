/*
 * Copyright (c) Fraunhofer FOKUS
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package io.piveau.hub.search.security;

import io.piveau.hub.search.*;
import io.piveau.json.*;
import io.piveau.security.*;
import io.vertx.core.*;
import io.vertx.core.json.*;
import io.vertx.ext.web.*;
import io.vertx.ext.web.client.*;
import org.slf4j.*;

/**
 * EXPERIMENTAL
 * This entire feature is proof-of-concept. Do not use in production.
 */
public class AccessControlHandler {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private Vertx vertx;
    private PiveauAuthConfig piveauAuthConfig;
    private KeycloakResourceHelper keycloakResourceHelper;

    public AccessControlHandler(Vertx vertx, JsonObject config) {
        if (AccessControlUtils.isEnabled()) {
            this.vertx = vertx;
            JsonObject authorizationProcessData = ConfigHelper.forConfig(config)
                    .forceJsonObject(Constants.ENV_PIVEAU_HUB_AUTHORIZATION_PROCESS_DATA);

            piveauAuthConfig = new PiveauAuthConfig(authorizationProcessData);
            WebClient webClient = WebClient.create(vertx);
            KeycloakTokenServerConfig keycloakTokenServerConfig = (KeycloakTokenServerConfig) piveauAuthConfig.getTokenServerConfig();

            keycloakResourceHelper = new KeycloakResourceHelper(webClient, keycloakTokenServerConfig);
        }
    }

    public void handleAccessControl(RoutingContext ctx) {
        if (AccessControlUtils.isEnabled()) {
            String token = ctx.request().headers().get("X-User-Token");
            LOGGER.info("Called Access Control Search Handler");
            if (token == null) {
                AccessControlContext accessControlContext = new AccessControlContext();
                ctx.put("acCtx", accessControlContext);
                ctx.next();
            } else {
                getPermissions(token).onSuccess(permissions -> {
                    LOGGER.info(permissions.encodePrettily());
                    AccessControlContext accessControlContext = new AccessControlContext(token, permissions);
                    ctx.put("acCtx", accessControlContext);
                    ctx.next();
                }).onFailure(error -> {
                    LOGGER.info("User was not found");
                    LOGGER.error(error.getMessage());
                    AccessControlContext accessControlContext = new AccessControlContext();
                    ctx.put("acCtx", accessControlContext);
                    ctx.next();
                });
            }
        } else {
            ctx.next();
        }
    }

    private Future<JsonArray> getPermissions(String token) {
        return keycloakResourceHelper.getAllPermissions(token);
    }
}
