package io.piveau.hub.search.services.catalogues;

import io.piveau.hub.search.Constants;
import io.piveau.hub.search.Helper;
import io.piveau.hub.search.services.vocabulary.VocabularyService;
import io.piveau.hub.search.util.geo.SpatialChecker;
import io.piveau.hub.search.util.index.IndexManager;
import io.piveau.hub.search.util.request.QueryParams;
import io.piveau.hub.search.util.response.ReturnHelper;
import io.piveau.hub.search.util.search.SearchClient;
import io.piveau.hub.search.util.search.SearchClientImpl.Elasticsearch.SearchResponseHelper;
import io.piveau.utils.PiveauContext;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.json.pointer.JsonPointer;
import io.vertx.serviceproxy.ServiceException;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CataloguesServiceImpl implements CataloguesService {

    private final SearchClient searchClient;

    private final VocabularyService vocabularyService;

    private final Cache<String, JsonObject> cache;

    private final PiveauContext serviceContext;
    private static final String STR_IS_PART_OF = "is_part_of";
    private static final String STR_HAS_PART = "has_part";
    private static final String STR_CATALOG = "catalog";
    private static final String RESULT = "result";
    private static final String COUNTRY = "country";
    private static final String ID_RAW = ".id.raw";

    CataloguesServiceImpl(Vertx vertx, JsonObject config, IndexManager indexManager, Promise<CataloguesService> promise) {
        searchClient = SearchClient.build(vertx, config, indexManager);
        vocabularyService = VocabularyService.createProxy(vertx, VocabularyService.SERVICE_ADDRESS);

        serviceContext = new PiveauContext("hub.search", "CataloguesService");

        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder().withCache("catalogueData",
                        CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, JsonObject.class,
                                        ResourcePoolsBuilder.newResourcePoolsBuilder().heap(200, EntryUnit.ENTRIES))
                                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofHours(1))))
                .build(true);

        cache = cacheManager.getCache("catalogueData", String.class, JsonObject.class);

        promise.complete(this);
    }

    @Override
    public Future<JsonArray> listCatalogues(String alias) {
        String defaultAlias = alias == null ? Constants.getReadAlias(Constants.DOC_TYPE_CATALOGUE) : alias;

        JsonObject query = new JsonObject()
                .put("size", 10000)
                .put("filter", Constants.DOC_TYPE_CATALOGUE)
                .put("aggregation", false)
                .put("includes", new JsonArray().add("id"))
                .put("scroll", true)
                .put("alias", defaultAlias);

        return searchClient.listIds(query.mapTo(QueryParams.class), false, true);
    }

    @Override
    public Future<JsonObject> createCatalogue(JsonObject payload) {
        PiveauContext resourceContext = serviceContext.extend("id-not-available");
        SpatialChecker.check(payload);
        putSuperCatalogueIdInPayload(payload);
        putSubCatalogueIdsInPayload(payload);

        return vocabularyService.replaceVocabularyInPayload(payload)
                .compose(result ->
                        searchClient.postDocument(Constants.DOC_TYPE_CATALOGUE, false, payload)
                                .onSuccess(v -> resourceContext.log().debug("Post success: " + payload))
                                .map(ReturnHelper.returnSuccess(201, new JsonObject().put("id", result)))
                );
    }

    @Override
    public Future<JsonObject> createOrUpdateCatalogue(String catalogueId, JsonObject payload) {
        Promise<JsonObject> promise = Promise.promise();
        PiveauContext resourceContext = serviceContext.extend(catalogueId);
        SpatialChecker.check(payload);
        putSuperCatalogueIdInPayload(payload);
        putSubCatalogueIdsInPayload(payload);
        vocabularyService.replaceVocabularyInPayload(payload).onComplete(replaceVocabularyInPayloadResult ->
                searchClient.putDocument(Constants.DOC_TYPE_CATALOGUE, catalogueId, false, payload)
                        .onSuccess(result -> {
                            resourceContext.log().debug("Put success: " + payload);
                            if (result == 200) {
                                // updated
                                resourceContext.log().info("Update catalogue: Catalogue {} updated.", catalogueId);
                                updateByQuery(catalogueId, payload, true);
                            } else {
                                // created
                                resourceContext.log().info("Create catalogue: Catalogue {} created.", catalogueId);
                            }
                            promise.complete(ReturnHelper.returnSuccess(result, new JsonObject().put("id", catalogueId)));
                        }).onFailure(failure -> {
                            resourceContext.log().error("Put failed: " + failure.getMessage());
                            promise.fail(failure);
                        }));
        return promise.future();
    }

    @Override
    public Future<JsonObject> modifyCatalogue(String catalogueId, JsonObject payload) {
        PiveauContext resourceContext = serviceContext.extend(catalogueId);
        resourceContext.log().debug("PATCH - Payload: {}", payload);
        SpatialChecker.check(payload);
        putSuperCatalogueIdInPayload(payload);
        putSubCatalogueIdsInPayload(payload);
        return vocabularyService.replaceVocabularyInPayload(payload)
                .compose(replaced ->
                        searchClient.patchDocument(Constants.DOC_TYPE_CATALOGUE, catalogueId, false, payload)
                                .compose(result -> {
                                    resourceContext.log().debug("Patch success: " + payload);
                                    resourceContext.log().info("Modify catalogue: Catalogue {} modified.", catalogueId);
                                    return updateByQuery(catalogueId, payload, false);
                                })
                                .onFailure(failure -> resourceContext.log().error("Patch failed: " + failure.getMessage()))
                                .map(ReturnHelper.returnSuccess(200, new JsonObject().put("id", catalogueId)))
                );
    }

    @Override
    public Future<JsonObject> readCatalogue(String catalogueId) {
        return readCatalogue(catalogueId, false, false)
                .compose(result ->
                        searchClient.countDocuments(Constants.DOC_TYPE_DATASET, STR_CATALOG+ID_RAW, catalogueId)
                                .onSuccess(count -> result.getJsonObject(RESULT).put("count", count))
                                .onFailure(cause -> result.getJsonObject(RESULT).putNull("count"))
                                .map(result)
                );
    }

    @Override
    public Future<Void> deleteCatalogue(String catalogueId) {
        return searchClient.deleteDocument(Constants.DOC_TYPE_CATALOGUE, catalogueId, false)
                .compose(result -> searchClient.deleteByQuery(
                        Constants.DOC_TYPE_DATASET,
                        STR_CATALOG+ID_RAW,
                        catalogueId)
                );
    }

    @Override
    public Future<JsonArray> replaceCatalogueInItems(JsonArray items) {

        List<Future<Void>> futures = items.stream()
                .filter(Objects::nonNull)
                .map(JsonObject.class::cast)
                .map(catalogue -> {
                    String catalogueId = catalogue.getString("id", "");
                    return readCatalogue(catalogueId, false, true)
                            .onSuccess(result -> {
                                JsonObject readCatalogue = result.getJsonObject(RESULT);
                                catalogue.put("title", SearchResponseHelper.getCatalogTitle(readCatalogue));
                            })
                            .<Void>mapEmpty();
                })
                .toList();

        return Future.all(futures).map(items);
    }

    @Override
    public Future<JsonArray> replaceCatalogueInResultList(JsonArray payload, List<String> includes) {

        List<Future<JsonObject>> futures = payload.stream()
                .map(JsonObject.class::cast)
                .map(dataset -> replaceCatalogueInResponse(dataset, includes))
                .toList();

        return Future.all(futures)
                .map(c -> {
                    List<JsonObject> results = futures.stream()
                            .filter(Future::succeeded)
                            .map(Future::result)
                            .toList();
                    return new JsonArray(results);
                });
    }

    @Override
    public Future<JsonObject> replaceCatalogueInResponse(JsonObject dataset, List<String> includes) {
        Promise<JsonObject> promise = Promise.promise();
        try {
            JsonObject catalog = dataset.getJsonObject(STR_CATALOG);
            if (Helper.isNullOrEmpty(catalog)) {
                promise.fail(new ServiceException(400, "Catalog missing"));
                return promise.future();
            }

            String catalogueId = dataset.getJsonObject(STR_CATALOG).getString("id");
            readCatalogue(catalogueId, false, true)
                    .onSuccess(ar -> {
                        JsonObject readCatalog = ar.getJsonObject(RESULT);
                        if (!Helper.isNullOrEmpty(includes)) {
                            JsonObject filteredCatalog = new JsonObject();
                            for (String include : includes) {
                                String jsonPath = '/' + include.replace('.', '/');
                                JsonPointer jsonPointer = JsonPointer.from(jsonPath);
                                Object result = jsonPointer.queryJson(readCatalog);
                                if (result != null) {
                                    jsonPointer.writeJson(filteredCatalog, result, true);
                                }
                            }
                            readCatalog = filteredCatalog;
                        }
                        dataset.put(STR_CATALOG, readCatalog);
                        promise.complete(dataset);
                    })
                    .onFailure(cause -> {
                        if (cause instanceof ServiceException se) {
                            promise.fail(new ServiceException(se.failureCode(), se.getMessage()));
                        } else {
                            promise.fail(new ServiceException(500, cause.getMessage()));
                        }
                    });
        } catch (ClassCastException e) {
            promise.fail(new ServiceException(500, "Catalog not json"));
        }

        return promise.future();
    }

    @Override
    public Future<JsonArray> checkCatalogueInPayloadArray(JsonArray payload) {
        List<Future<Void>> futures = payload.stream()
                .map(JsonObject.class::cast)
                .map(obj -> checkCatalogueInPayloadObject(obj).<Void>mapEmpty())
                .toList();

        return Future.all(futures).map(payload);
    }

    @Override
    public Future<JsonObject> checkCatalogueInPayloadObject(JsonObject payload) {
        JsonObject catalog = payload.getJsonObject(STR_CATALOG);
        if (catalog != null && !catalog.isEmpty()) {
            String catalogueId = payload.getJsonObject(STR_CATALOG).getString("id");

            return readCatalogue(catalogueId, true, true)
                    .map(result -> {
                        JsonObject readCatalog = result.getJsonObject(RESULT);
                        payload.getJsonObject(STR_CATALOG).put(STR_IS_PART_OF, readCatalog.getString(STR_IS_PART_OF));
                        payload.put(COUNTRY, readCatalog.getJsonObject(COUNTRY));

                        return payload;
                    });
        } else {
            return Future.failedFuture(new ServiceException(400, "Catalog missing"));
        }
    }

    private Future<JsonObject> readCatalogue(String catalogueId, boolean useWriteAlias, boolean useCache) {
        if (useCache && cache.containsKey(catalogueId)) {
            return Future.succeededFuture(ReturnHelper.returnSuccess(200, cache.get(catalogueId)));
        } else {
            String alias = useWriteAlias ? Constants.getWriteAlias(Constants.DOC_TYPE_CATALOGUE) :
                    Constants.getReadAlias(Constants.DOC_TYPE_CATALOGUE);
            return searchClient.getDocument(Constants.DOC_TYPE_CATALOGUE, alias, catalogueId, false)
                    .map(result -> {
                        cache.put(catalogueId, result);
                        return ReturnHelper.returnSuccess(200, result);
                    });
        }
    }

    private Future<Void> updateByQuery(String catalogueId, JsonObject payload, boolean replaceAll) {
        List<String> globalReplacements = new ArrayList<>();
        globalReplacements.add(COUNTRY);

        List<String> fieldReplacements = List.of("title", "description", COUNTRY);

        return searchClient.updateByQuery(Constants.DOC_TYPE_DATASET, STR_CATALOG+ID_RAW, catalogueId, STR_CATALOG,
                globalReplacements, fieldReplacements, payload, replaceAll);
    }

    private void putSuperCatalogueIdInPayload(JsonObject payload) {
        String superCatalogue = payload.getString(STR_IS_PART_OF);
        if (superCatalogue != null) {
            payload.put(STR_IS_PART_OF, superCatalogue.substring(superCatalogue.lastIndexOf('/') + 1));
        }
    }

    private void putSubCatalogueIdsInPayload(JsonObject payload) {
        JsonArray hasPart = payload.getJsonArray(STR_HAS_PART);
        if (hasPart != null) {
            JsonArray subCatalogues = new JsonArray();
            for (int i = 0; i < hasPart.size(); i++) {
                String uriRef = hasPart.getString(i);
                subCatalogues.add(uriRef.substring(uriRef.lastIndexOf('/') + 1));
            }
            payload.put(STR_HAS_PART, subCatalogues);
        }
    }

}
