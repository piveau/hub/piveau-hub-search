package io.piveau.hub.search.services.sitemaps;

import io.piveau.hub.search.Constants;
import io.piveau.hub.search.util.index.IndexManager;
import io.piveau.hub.search.util.request.QueryParams;
import io.piveau.hub.search.util.response.ReturnHelper;
import io.piveau.hub.search.util.search.SearchClient;
import io.piveau.hub.search.util.sitemap.Sitemap;
import io.piveau.hub.search.util.sitemap.SitemapIndex;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileProps;
import io.vertx.core.file.FileSystemException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class SitemapsServiceImpl implements SitemapsService {

    private final SearchClient searchClient;

    private static final String PATH1 = "./sitemaps/1/";
    private static final String PATH2 = "./sitemaps/2/";

    private static final String SITEMAP_INDEX_FILE = "sitemap_index.xml";

    // path for sitemap xml
    private String writePath;
    private String readPath;

    // sitemap config
    private final JsonObject sitemapConfig;

    // vertx
    private final Vertx vertx;

    private static final Logger LOG = LoggerFactory.getLogger(SitemapsServiceImpl.class);

    SitemapsServiceImpl(Vertx vertx, JsonObject esConfig, IndexManager indexManager, JsonObject sitemapConfig,
                        Handler<AsyncResult<SitemapsService>> handler) {

        this.searchClient = SearchClient.build(vertx, esConfig, indexManager);

        this.sitemapConfig = sitemapConfig;
        this.vertx = vertx;

        vertx.fileSystem().mkdirs(PATH1)
                .compose(v -> vertx.fileSystem().mkdirs(PATH2))
                .compose(v -> setReadWriteSwitch())
                .compose(v -> clearDirectory(writePath))
                .onSuccess(v -> {
                    scheduleSitemapsGeneration();
                    handler.handle(Future.succeededFuture(this));
                })
                .onFailure(cause -> {
                    LOG.error("Sitemaps folder structure initialization failed", cause);
                    handler.handle(Future.failedFuture(cause));
                });
    }

    private void scheduleSitemapsGeneration() {
        Long interval = sitemapConfig.getLong("interval", 86400000L);
        if (interval > 0) {
            LOG.info("Sitemap generation interval: {} ms", interval);
            vertx.setPeriodic(0, interval, periodic -> generateSitemaps());
        } else if (interval == 0) {
            LOG.info("Sitemap generation only at startup");
            // slightly delayed
            vertx.setTimer(1000, id -> generateSitemaps());
        } else {
            LOG.info("Sitemap generation disabled");
        }
    }

    private Future<Void> clearDirectory(String directory) {
        return vertx.fileSystem().readDir(writePath)
                .compose(files -> {
                    List<Future<Void>> futures = files.stream().map(file -> vertx.fileSystem().delete(file)).toList();
                    return Future.join(futures);
                })
                .recover(cause -> null)
                .mapEmpty();
    }

    private Future<Void> setReadWriteSwitch() {
        Future<Long> path1 = vertx.fileSystem().props(PATH1 + SITEMAP_INDEX_FILE).map(FileProps::creationTime);
        Future<Long> path2 = vertx.fileSystem().props(PATH2 + SITEMAP_INDEX_FILE).map(FileProps::creationTime);
        return Future.join(path1, path2)
                .onSuccess(cf -> {
                    readPath = path1.result() < path2.result() ? PATH2 : PATH1;
                    writePath = readPath.equals(PATH1) ? PATH2 : PATH1;
                })
                .onFailure(cause -> {
                    if (path1.failed() && path2.succeeded()) {
                        readPath = PATH2;
                        writePath = PATH1;
                    } else {
                        readPath = PATH1;
                        writePath = PATH2;
                    }
                })
                .otherwiseEmpty()
                .mapEmpty();
    }

    @Override
    public Future<JsonObject> readSitemapIndex() {
        return readFile(SITEMAP_INDEX_FILE);
    }

    @Override
    public Future<JsonObject> readSitemap(String sitemapId) {
        return readFile("sitemap_datasets_" + sitemapId + ".xml");
    }

    private Future<JsonObject> readFile(String filename) {
        return vertx.fileSystem()
                .readFile(readPath + filename)
                .map(buffer -> ReturnHelper.returnSuccess(200, buffer.toString()))
                .recover(cause -> {
                    if (cause instanceof FileSystemException && cause.getCause() instanceof NoSuchFileException) {
                        throw new ServiceException(404, "Sitemap file " + filename + " not found");
                    } else {
                        throw new ServiceException(500, cause.getMessage());
                    }
                });
    }

    @Override
    public Future<JsonObject> triggerSitemapGeneration() {
        return generateSitemaps()
                .map(v -> ReturnHelper.returnSuccess(200, "Triggered sitemap generation"));
    }

    private Future<Void> generateSitemaps() {
        int chunkSize = sitemapConfig.getInteger("size", 10000);
        return listDatasets(chunkSize)
                .compose(result -> {
                    int amount = result.size();
                    List<Future<Void>> futures = new ArrayList<>(IntStream.range(0, amount)
                            .mapToObj(i -> {
                                String sitemap = createSitemap(result.getJsonArray(i));
                                return vertx.fileSystem().writeFile(writePath + "sitemap_datasets_" + (i + 1) + ".xml",
                                                Buffer.buffer(sitemap.getBytes()))
                                        .onFailure(cause -> LOG.error("Sitemap write", cause));
                            })
                            .toList());

                    String sitemapIndex = createSitemapIndex(amount);
                    Future<Void> indexFuture = vertx.fileSystem().writeFile(writePath + SITEMAP_INDEX_FILE, Buffer.buffer(sitemapIndex.getBytes()))
                            .onSuccess(v -> LOG.info("Sitemap: written sitemap index to: {}/sitemap_index.xml", writePath))
                            .onFailure(cause -> LOG.error("Sitemap index write", cause));

                    futures.add(indexFuture);

                    return Future.all(futures).<Void>mapEmpty();
                })
                .onSuccess(v -> {
                    readPath = writePath;
                    writePath = writePath.equals(PATH1) ? PATH2 : PATH1;
                })
                .onFailure(failure -> LOG.error("Sitemap list datasets", failure));
    }

    private String createSitemapIndex(int count) {

        SitemapIndex sitemapIndex = new SitemapIndex();

        //`static` should be used, but `drupal` was the name before it, so it remains as fallback
        sitemapIndex.addSitemap(sitemapConfig.getString("static", sitemapConfig.getString("drupal", "")));

        for (int i = 0; i < count; ++i) {
            sitemapIndex.addSitemap(sitemapConfig.getString("url", "")
                    + "sitemap_datasets_" + (i + 1) + ".xml");
        }

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(SitemapIndex.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(sitemapIndex, sw);
            // DEBUG: jaxbMarshaller.marshal(feed, System.out);

            return sw.toString();
        } catch (JAXBException e) {
            LOG.error("Sitemap index marshalling", e);
            return "";
        }
    }

    private String createSitemap(JsonArray entries) {

        Sitemap sitemap = new Sitemap();

        for (Object obj : entries) {
            JsonObject entry = (JsonObject) obj;

            // this was made configurable, it was `en` by default, now it still is, if nothing else is configured for backwards compatibility
            String language = sitemapConfig.getString("defaultLanguage", "en");

            sitemap.addSitemap(
                    sitemapConfig.getString("url", "") + "datasets" + "/" + entry.getString("id"),
                    entry.getJsonObject("catalog_record", new JsonObject()).getString("modified"),
                    sitemapConfig.getString("changefreq", "never"),
                    language,
                    sitemapConfig.getJsonArray("languages", new JsonArray())
            );
        }

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Sitemap.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(sitemap, sw);
            // DEBUG: jaxbMarshaller.marshal(feed, System.out);

            return sw.toString();
        } catch (JAXBException e) {
            LOG.error("Sitemap marshalling", e);
            return "";
        }
    }

    private Future<JsonArray> listDatasets(Integer chunkSize) {
        JsonObject q = new JsonObject();
        q.put("size", chunkSize);
        q.put("filter", Constants.DOC_TYPE_DATASET);
        q.put("aggregation", false);
        q.put("includes", new JsonArray().add("id").add("catalog_record.modified"));
        q.put("scroll", true);

        QueryParams query = Json.decodeValue(q.toString(), QueryParams.class);

        return searchClient.listIds(query, true, false);
    }

}
