package io.piveau.hub.search.UnitTest;

import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Operator;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import io.piveau.hub.search.Constants;
import io.piveau.hub.search.util.request.QueryParams;
import io.piveau.hub.search.util.search.SearchClientImpl.Elasticsearch.SearchQueryBuilder;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertFalse;

@DisplayName("Testing the SearchRequestBuilder")
@ExtendWith(VertxExtension.class)
class SearchRequestBuilderTest {
    private static final Logger logger = LoggerFactory.getLogger(SearchRequestBuilderTest.class);
    @BeforeEach
    void setUp(Vertx vertx, VertxTestContext testContext) {
        testContext.completeNow();
    }

    @AfterEach
    void tearDown(Vertx vertx, VertxTestContext testContext) {
        testContext.completeNow();
    }

    @Test
    @DisplayName("Check the fields to search in")
    void testSearchInFields(Vertx vertx, VertxTestContext testContext) {
        List<String> searchInFields = new ArrayList<>();
        QueryParams query = new QueryParams();
        query.setQ("quick brown");

        BoolQuery boolQuery = SearchQueryBuilder.buildQuery(query, new JsonObject(), Operator.Or);
        for (Query q : boolQuery.must() ) {
            searchInFields = q.multiMatch().fields();
        }
        List<String> finalSearchInFields = searchInFields;

        logger.info("/search searches in fields: "+finalSearchInFields.toString());
        testContext.verify(() -> assertFalse(finalSearchInFields.isEmpty()));
        testContext.completeNow();
    }

    @Test
    @DisplayName("Check the fields to search in when using filter query parameter")
    void testSearchInFieldsForFilter(Vertx vertx, VertxTestContext testContext) {
        List<String> searchInFields = new ArrayList<>();
        List<String> docTypes = Arrays.asList(Constants.DOC_TYPE_CATALOGUE, Constants.DOC_TYPE_DATASET);

        QueryParams query = new QueryParams();
        query.setQ("quick brown");

        for (String docType : docTypes) {
            query.setFilter(docType);
            BoolQuery boolQuery = setQuery(vertx, docType, query);
            for (Query q : boolQuery.must() ) {
                searchInFields = q.multiMatch().fields();
            }
            List<String> finalSearchInFields = searchInFields;

            logger.info("?filter="+docType+" searches in fields: "+finalSearchInFields.toString());
            testContext.verify(() -> assertFalse(finalSearchInFields.isEmpty()));
        }

        testContext.completeNow();
    }

    @Test
    @DisplayName("Check the fields to search in when doing wildcard or regex search")
    void testSearchInFieldsInWildcardOrRegexSearch(Vertx vertx, VertxTestContext testContext) {
        List<String> searchInFields = new ArrayList<>();
        List<String> docTypes = Arrays.asList(Constants.DOC_TYPE_CATALOGUE, Constants.DOC_TYPE_DATASET);

        QueryParams query = new QueryParams();
        query.setQ("qu*on");

        for (String docType : docTypes) {
            query.setFilter(docType);
            BoolQuery boolQuery = setQuery(vertx, docType, query);
            for (Query q : boolQuery.must() ) {
                searchInFields = q.queryString().fields();
            }
            List<String> finalSearchInFields = searchInFields;

            logger.info("?filter="+docType+"&q="+query.getQ()+" searches in fields: "+finalSearchInFields.toString());
            testContext.verify(() -> assertFalse(finalSearchInFields.isEmpty()));
        }

        testContext.completeNow();
    }

    private BoolQuery setQuery(Vertx vertx, String docType, QueryParams query) {
        JsonObject config = new JsonObject(vertx.fileSystem()
                .readFileBlocking(docType+"_config.json")
                .toString());

        return SearchQueryBuilder.buildQuery(query,
                new JsonObject(),
                Operator.Or,
                config.getJsonObject("boost"),
                getDocFacets(config),
                getDocSearchParams(config));
    }

    private Map<String, JsonObject> getDocFacets(JsonObject config) {
        Map<String, JsonObject> facets = new HashMap<>();
        JsonArray facetsArray = config.getJsonArray("facets");
        for (int i = 0; i < facetsArray.size(); i++) {
            JsonObject param = facetsArray.getJsonObject(i);
            String name = param.getString("name");
            facets.put(name, param);
        }

        return facets;
    }

    private Map<String, JsonObject> getDocSearchParams(JsonObject config) {
        Map<String, JsonObject> searchParams = new HashMap<>();
        JsonArray searchParamsArray = config.getJsonArray("searchParams");
        for (int i = 0; i < searchParamsArray.size(); i++) {
            JsonObject param = searchParamsArray.getJsonObject(i);
            String name = param.getString("name");
            searchParams.put(name, param);
        }

        return searchParams;
    }

}
