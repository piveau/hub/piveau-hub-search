package io.piveau.hub.search.UnitTest;

import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.elasticsearch.core.search.ResponseBody;
import co.elastic.clients.json.JsonData;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import io.piveau.hub.search.util.request.QueryParams;
import io.piveau.hub.search.util.search.SearchClientImpl.Elasticsearch.SearchResponseHelper;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.joda.time.DateTime;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Testing the SearchResponseHelper")
@ExtendWith(VertxExtension.class)
class SearchResponseHelperTest {

    private final Logger LOG = LoggerFactory.getLogger(SearchResponseHelperTest.class);

    @BeforeEach
    void setUp(Vertx vertx, VertxTestContext testContext) {
        testContext.completeNow();
    }

    @AfterEach
    void tearDown(Vertx vertx, VertxTestContext testContext) {
        testContext.completeNow();
    }

    @Test
    @DisplayName("Testing basic processSearchResult with correct modified and issued")
    void testProcessSearchResultWithCorrectDates(Vertx vertx, VertxTestContext testContext) {
        try {
            String modified = "_" + DateTime.now().plusYears(1).toString();
            String issued = "_" + DateTime.now().plusYears(1).toString();

            JsonData source = JsonData.fromJson("{\n" +
                    "                \"id\" : \"test-id-1\",\n" +
                    "                \"modified\" : \"" + modified +"\",\n" +
                    "                \"issued\" : \"" + issued +"\"\n" +
                    "            }");
            JsonData source1 = JsonData.of(source, new JacksonJsonpMapper());

            Map<String, JsonData> fields = new HashMap<>();
            JsonData ignored = JsonData.of(
                    Arrays.asList("modified", "issued"),
                    new JacksonJsonpMapper());
            fields.put("_ignored", ignored);

            HitsMetadata<JsonData> hitsMetadata = HitsMetadata.of(hm ->
                    hm.hits(hit -> hit
                            .id("test-id-1")
                            .index("test-index")
                            .type("_doc")
                            .fields(fields)
                            .source(source1)
                    ));
            ResponseBody<JsonData> searchResponse = SearchResponse.of(sr -> {
                return sr.took(1)
                        .timedOut(false)
                        .shards(s -> s.failed(0).successful(1).total(1))
                        .hits(hitsMetadata);
            });

            QueryParams query = Json.decodeValue("{\"from\": 0, \"size\": 10}", QueryParams.class);
            JsonArray results = SearchResponseHelper
                    .processSearchResult(searchResponse, query, null, null, null, null);

            JsonObject result = results.getJsonObject(0);

            assertEquals(modified.substring(1), result.getString("modified"));
            assertEquals(issued.substring(1), result.getString("issued"));

            testContext.completeNow();
        } catch (Exception e) {
            testContext.failNow(e);
        }
    }

    @Test
    @DisplayName("Testing basic processSearchResult with modified and issued in future")
    void testProcessSearchResultWithFutureDates(Vertx vertx, VertxTestContext testContext) {
        try {
            String modified = DateTime.now().toString();
            String issued = DateTime.now().toString();

            JsonData source = JsonData.fromJson("{\n" +
                    "                \"id\" : \"test-id-1\",\n" +
                    "                \"modified\" : \"" + modified +"\",\n" +
                    "                \"issued\" : \"" + issued +"\"\n" +
                    "            }");
            JsonData source1 = JsonData.of(source, new JacksonJsonpMapper());

            Map<String, JsonData> fields = new HashMap<>();
            JsonData ignored = JsonData.of(
                    Arrays.asList("modified", "issued"),
                    new JacksonJsonpMapper());
            fields.put("_ignored", ignored);

            HitsMetadata<JsonData> hitsMetadata = HitsMetadata.of(hm ->
                    hm.hits(hit -> hit
                            .id("test-id-1")
                            .index("test-index")
                            .type("_doc")
                            .fields(fields)
                            .source(source1)
                    ));
            ResponseBody<JsonData> searchResponse = SearchResponse.of(sr -> {
                return sr.took(1)
                        .timedOut(false)
                        .shards(s -> s.failed(0).successful(1).total(1))
                        .hits(hitsMetadata);
            });
            QueryParams query = Json.decodeValue("{\"from\": 0, \"size\": 10}", QueryParams.class);
            JsonArray results = SearchResponseHelper
                    .processSearchResult(searchResponse, query, null, null, null, null);

            JsonObject result = results.getJsonObject(0);

            assertEquals(modified, result.getString("modified"));
            assertEquals(issued, result.getString("issued"));

            testContext.completeNow();
        } catch (Exception e) {
            testContext.failNow(e);
        }
    }

    @Test
    @DisplayName("Testing basic processSearchResult with malformed modified and issued")
    void testProcessSearchResultWithMalformedDates(Vertx vertx, VertxTestContext testContext) {
        try {
            String modified = "malformed_modified";
            String issued = "malformed_issued";

            JsonData source = JsonData.fromJson("{\n" +
                    "                \"id\" : \"test-id-1\",\n" +
                    "                \"modified\" : \"" + modified +"\",\n" +
                    "                \"issued\" : \"" + issued +"\"\n" +
                    "            }");
            JsonData source1 = JsonData.of(source, new JacksonJsonpMapper());

            Map<String, JsonData> fields = new HashMap<>();
            JsonData ignored = JsonData.of(
                    Arrays.asList("modified", "issued"),
                    new JacksonJsonpMapper());
            fields.put("_ignored", ignored);

            HitsMetadata<JsonData> hitsMetadata = HitsMetadata.of(hm ->
                    hm.hits(hit -> hit
                            .id("test-id-1")
                            .index("test-index")
                            .type("_doc")
                            .fields(fields)
                            .source(source1)
                    ));
            ResponseBody<JsonData> searchResponse = SearchResponse.of(sr -> {
                return sr.took(1)
                        .timedOut(false)
                        .shards(s -> s.failed(0).successful(1).total(1))
                        .hits(hitsMetadata);
            });
            QueryParams query = Json.decodeValue("{\"from\": 0, \"size\": 10}", QueryParams.class);
            JsonArray results = SearchResponseHelper
                    .processSearchResult(searchResponse, query, null, null, null, null);

            JsonObject result = results.getJsonObject(0);

            assertEquals(modified, result.getString("modified"));
            assertEquals(issued, result.getString("issued"));

            testContext.completeNow();
        } catch (Exception e) {
            testContext.failNow(e);
        }
    }
}