# ChangeLog

## Unreleased

## 5.1.15 (2025-03-07)

**Changed:**
* Being more informative at startup about sitemap generation 
* Even more optimized sitemap generation code

## 5.1.14 (2025-03-06)

**Changed:**
* Sitemap interval configuration

**Added:**
* Sitemap changefreq configuration

## 5.1.13 (2025-03-05)

**Fixed:**
* Context failure handler trying to set negative response code

**Changed:**
* Vocabulary update by query queries for datasets containing the targeted fields

## 5.1.12 (2025-02-20)

**Changed:**
* CI Deployment

## 5.1.11 (2025-02-20)

**Fixed:**
* Yaml library logging issue

## 5.1.10 (2025-02-20)

**Added:**
* Introduced PUT/GET/DELETE `/datasets/{datasetId}/revisions/{revisionId}` endpoints to manage a dataset-revision

**Changed:**
* Updated autocomplete search result
* Configured vocabulary index to support Licensing Assistant 2.0
* Set `GET /datasets/{id}/{revision}` endpoint as deprecated
* Added `revision_number`, `revisions`, `visibility` to `catalog_record` in the ElasticSearch dataset mapping

**Fixed:**
* Refactored the XML source of spdx-checksum-algorithm vocabulary

## 5.1.9 (2024-10-21)

**Added:**
* Introduced HVD Level Setting
* Migrated Resource Config to piveau profile

**Fixed:**
* Resolved the search result issue when `?showScore=true`
* Fixed an issue introduced in release 5.1.6 where using the `filter` query parameter caused the fields in the Elasticsearch search request to be empty (`"fields": []`)

## 5.1.8 (2024-09-11)

**Added:**
* Support for piveau Profile Feature
* Support regular expressions for Elasticsearch Query string query

**Changed:**
* Autocomplete is now applied to `title`, `description` and `keywords` fields
* Removed support to pass API token via Authorization header

## 5.1.7 (2024-07-29)

**Fixed:**
* Fix a bug, where the `title` in `[facets.items]` of a search result is assigned to the `id`
* Set the range of facet scoring to integer

## 5.1.6 (2024-07-04)

**Changed:**
* Migrate search process from Elasticsearch High Level Rest Client (HLRC) to Java API Client

## 5.1.4 (2024-06-17)

**Fixed:**
* PATCHing a dataset set the is_hvd field to false

## 5.1.3 (2024-06-13)

**Changed:**
* Fallback to HVD mapping without need to reindex

## 5.1.2 (2024-06-11)

**Added:**

* Configuration for default search operator is now possible by setting the field `operator` to `AND` in elastic search
  config. The default value
  is `OR`.
* New config variable to set HTTP body size limit: PIVEAU_HUB_SEARCH_HTTP_BODY_LIMIT

## 5.1.1 (2024-06-10)

**Fixed:**
* Removed required fields in OpenAPI

## 5.1.0 (2024-06-10)

**Added:**
* Schema and OpenAPI Update to support DCAT-AP HVD

## 5.0.6 (2024-05-29)

**Changed:**

* Update project dependencies

**Fixed:**
* Redoc JavaScript for the API documentation

## 5.0.5 (2024-05-15)

**Changed:**
* Improve multiple search process by using [Multi search API](https://www.elastic.co/guide/en/elasticsearch/reference/7.17/search-multi-search.html)
* Migrate index creation process from Elasticsearch High Level Rest Client (HLRC) to Java API Client

## 5.0.4 (2024-02-01)

**Added:**
* Support for setting a "is_hvd" flag in Datasets
* HVD features to shape definition in Helm Chart
* Search for multiple document types simultaneously by using `filters` query parameter to define the desired document types to search for
* Datasets and Catalogues search result can be filtered using `superCatalog` facet

**Changed**
* `superCatalogue` query parameter in `/search` end-point is now deprecated

**Removed:**
* default API key configuration, service will not start without an API key configured

## 5.0.3 (2023-11-09)

**Added:**
* Endpoint to retrieve vocable based on URI ref

**Fixed:**
* Vocabulary resolution when no ID is present
* Vocabulary resolution based on URI ref
* Vocabulary resolution bug for payload with resource field
* Dataset's publisher aggregation bug when its resource is empty

**Changed**
* Sitemap feature has now more configurability and is better documented

## 5.0.2 (2023-07-06)

**Added:**
* Javadoc
* Endpoint to delete a vocabulary
* New options property for index configuration 
* Support for enabling most_fields as type via options (https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-multi-match-query.html)
* Add Elasticsearch Java API Client
* Add `superCatalogue` as a new query parameter for GET `/search` request
* Add `superCatalog` as a new facet to filter catalogue search result

**Fixed:**
* Fixed bulk update of Vocabularies

## 5.0.1 (2023-07-06)

**Changed:**
* Restored POST Search endpoint

## 5.0.0 (2023-06-26)

**Added:**
* Generic resource endpoint based on shacl shapes
* Openapi generation based on shacl shapes

**Changed:**
* New SearchClient interface for modular search backends
* Vocabulary replacement, replace all occurrences of the field, includes/excludes parameters
* Bul update refactoring

## 4.2.0 (2023-03-16)

**Added:**
* Wildcard search, using `?` to replace a single character, and `*` to replace zero or more characters.
* Exact phrase search, using double quotes: `"This exact phrase in exactly the same order"`.

## 4.1.1 (2023-03-06)

**Changed:**
* non-blocking listIds function
* increase search service proxy timeout to 500000ms

## 4.1.0 (2023-02-28)

**Added:**
* `dcat:endpointDescription`, `dct:accessRights` and `dct:license` to access service in dataset mapping
* `alt_label` in vocabulary mapping
* `listCommands` command for cli
* `exists` query parameter for returning documents which have certain fields existent

**Changed:**
* Refactored shell service
* Camelcase for all cli commands

## 4.0.10 (2023-01-18)

**Changed:**
* Use catalog_record.modified for lastmod in sitemap
* Non-blocking sitemap generation

## 4.0.9 (2022-11-28)

**Changed:**
* Skip all future dates for lastmod in sitemap

## 4.0.8 (2022-11-17)

**Changed:**
* Default aggregation size to 250

## 4.0.7 (2022-11-10)

**Added:**
* Lastmod in sitemap
* Term query on a arbitrary field

## 4.0.6 (2022-09-17)

**Fixed:**
* Modify dataset bug (hardcoded govdata catalog)

## 4.0.5 (2022-09-15)

**Changed:**
* Multilingual mapping for categories and subject, vocabulary resolution on index time 
* Id hashing to allow long ids

## 4.0.4 (2022-07-07)

**Fixed:**
* Dataset revision rollover mapping/settings

## 4.0.3 (2022-05-02)

**Fixed:**
* Feed, use first element in access_url array

## 4.0.2 (2022-04-26)

**Added:**
* Create index command can specify number of shards

**Changed:**
* Disabled catalog update by query

**Fixed:**
* Geo query

## 4.0.1 (2022-04-13)

**Changed:**
* Use scroll for listIds

## 4.0.0 (2022-04-12)

**Changed:**
* Use elastic restclient 7.17.2 
* Use breaker for every request
* Fixed bulk request, remove bulk post request

## 3.0.4 (2022-04-04)

**Changed:**
* Increase elastic socket timeout to 60s and connection timeout to 5s

## 3.0.3 (2022-04-04)

**Changed:**
* Show date and time for logging
* Do not print payload for index failure

## 3.0.2 (2022-02-22)

**Added**
* List dataset and catalogue ids endpoint

## 3.0.1 (2022-02-21)

**Changed**
* Increase minimum and maximum of edge gram tokenizer for autocomplete

## 3.0.0 (2022-02-18)

**Changed**
* Use ServiceException class
* Increase eventbus timeouts
* Don't update dataset during PUT/PATCH in case of an elasticsearch failure

## 2.0.16 (2022-02-11)

**Changed**
* Reduce logging

## 2.0.15 (2022-02-09)

**Added**
* List vocabularies endpoint
* Mapping update for dataset resourece (UriRef) and extended metadata 

## 2.0.14 (2022-01-26)

**Added**
* Server in OpenApi to fix API call examples, see https://github.com/Redocly/redoc/issues/1172 

## 2.0.13 (2021-12-15)

**Changed**
* Update log4j-to-slf4j to 2.16 due to log4jshell bug

## 2.0.12 (2021-12-10)

**Changed:**
* Autocomplete feature is now available via query parameter `autocomplete`
* Return first error code of bulk vocabulary indexing

**Added:**
* `in_scheme` to vocabulary mapping

## 2.0.11 (2021-12-03)

**Changed:**
* Same behavior with query parameter `dataServices=false` and not set

## 2.0.10 (2021-12-02)

**Added:**
* Update mapping of dcat:accessService: title, description, endpointURL

## 2.0.9 (2021-11-25)

**Added:**
* First draft of vocabulary search

**Changed:**
* More generic term aggregation facet, e.g. publisher

## 2.0.8 (2021-11-03)
**Fixed:**
* feed item date now based on catalogue record data

## 2.0.7 (2021-10-28)
**Fixed:**
* link in RSS feed to dataset version

## 2.0.6 (2021-10-28)
**Added**
* endpoint for each dataset version

## 2.0.5 (2021-10-25)

**Added**
* single dataset history
* rss feed for single dataset history

## 2.0.4 (2021-10-22)

**Added:**
* contact_point.url

## 2.0.3 (2021-10-21)

**Added:**
* StatDCAT-AP support

## 2.0.2 (2021-10-14)

**Changed:**
* Ignore language with missing ID for payload processing

## 2.0.1 (2021-10-13)

**Changed:**
* Allow write alias for max result window command

## 2.0.0 (2021-10-12)

**Changed:**
* Extended schema to DCAT AP 2.0.1
* Changed scheme to use label instead of title for all non multi-language fields  

## 1.3.0

**Added:**

* Added catalogRecord issued and modified to the search index
* New mapping for foaf:page
* Reindex process via alias

## 1.2.13

**Fixed:**

* Boosting and scoring

**Changed:**

* Gazetteer config is not required anymore

**Added:**

* showScore query parameter
* OpenApi `spatial_resource`
* OpenApi GDPR complience

## 1.2.12

**Fixed:**

* Gazetteer geometry bug

## 1.2.11

**Changed:**

* Remove leading slash for imprint and privacy page

## 1.2.10

**Added:**

* OpenApi GDPR compliance

**Changed:**

* Set globalAggregation false by default

## 1.2.9

**Added:**

* Title caching of facet titles
* Allow exact filtering via "includes" of subfields of catalog
* Fallback mechanism for langauge based fields for "includes"

**Changed:**

* Use of Elasticsearch 7.10.2
* PUT preserves stored score, if not set

## 1.2.8

**Removed:**
* Title requirements in Open API

## 1.2.7

**Added:**
* CLI command number of replica per index

**Changed:**
* Default config for data scope facet

## 1.2.6

**Added:**
* Supporting configurable favicon and logo

## 1.2.5

**Changed:**

* Improved logging with PiveauContext

## 1.2.4

**Added:**

* PiveauContext logging
* ELK stack configuration
* Field contact_point/address in dataset mapping
* Field contact_point/telepone in dataset mapping

## 1.2.3

**Added:**

* Missing artifact LICENSE.md
* Must match and must not match aggregation
* Caching for vocabulary and catalog replacement in dataset requests
* Vocabulary index and endpoint
* Field dct:deadline in dataset mapping
* Field dct:type in dataset mapping

**Changed:**

* Increase breaker tries in standard config

## 1.2.2

Era Before changelog
